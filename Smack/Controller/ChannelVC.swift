//
//  ChannelVC.swift
//  Smack
//
//  Created by Martin Rojas on 3/10/18.
//  Copyright © 2018 Y.M. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController {

    @IBOutlet weak var LoginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
    }
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        performSegue(withIdentifier: TO_LOGIN, sender: nil)
    }
    
}
